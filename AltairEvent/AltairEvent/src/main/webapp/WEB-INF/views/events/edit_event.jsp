<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Event</title>
</head>
<body>
	<div align="center">
		<h2>New Event</h2>
		<form:form action="saveEvents" method="post" modelAttribute="events">
			<table border="0" cellpadding="5">
				<tr>
					<td>ID:</td>
					<td>${events.id}<form:hidden path="id" />
					</td>
				</tr>
				<tr>
					<td>Start Date:</td>
					<td><form:input path="startDate" /></td>
				</tr>
				<tr>
					<td>End Date:</td>
					<td><form:input path="endDate" /></td>
				</tr>
				<tr>
					<td>Assistance:</td>
					<td><form:input path="assistance" /></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>Organization Company:</td>
					<td><form:input path="organizationCompany" /></td>
				</tr>
				<tr>
					<td>Total Price:</td>
					<td><form:input path="totalPrice" /></td>
				</tr>

				<tr>
					<td>Services:</td>
					<td><form:input path="services" /></td>
				</tr>
				<tr>
					<td>Commercial:</td>
					<td><form:input path="commercial" /></td>
				</tr>
				<tr>
					<td>Customer:</td>
					<td><form:input path="customer" /></td>
				</tr>
				<tr>
					<td>Saloon:</td>
					<td><form:input path="saloon" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>