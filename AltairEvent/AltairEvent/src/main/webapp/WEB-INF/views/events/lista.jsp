<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALtair Event</title>
</head>
<body>
<div align="center">
    <h2>ALtair Event. Events</h2>
    <form method="get" action="searchEvents">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Events" />
    </form>
    <h3><a href="newEvents">New Events</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>StartDate</th>
            <th>EndDate</th>
            <th>Assistance</th>
            <th>Name</th>
            <th>Organization Company</th>
            <th>Total Price</th>
			<th>Services</th>
            <th>Commercial</th>
            <th>Customer</th>
            <th>Saloon</th>
        </tr>
        <c:forEach items="${listEvents}" var="events">
        <tr>
            <td>${events.startDate}</td>
            <td>${events.endDate}</td>
            <td>${events.assistance}</td>
            <td>${events.name}</td>
            <td>${events.organizationCompany}</td>
            <td>${events.totalPrice}</td>
            <td>${events.services}</td>
            <td>${events.commercial}</td>
            <td>${events.customer}</td>
            <td>${events.saloon}</td>
            <td>
                <a href="editEvents?id=${events.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="deleteEvents?id=${events.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>