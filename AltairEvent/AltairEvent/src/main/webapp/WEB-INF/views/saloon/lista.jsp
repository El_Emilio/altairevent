<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALtair Event</title>
</head>
<body>
<div align="center">
    <h2>ALtair Event. Saloon</h2>
    <form method="get" action="searchSaloon">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="newSaloons">New Saloon</a>    </h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Capacity</th>
           <th>Available</th>
            <th>Price</th>
        </tr>
        <c:forEach items="${listSaloon}" var="saloon">
        <tr>
            <td>${saloon.id}</td>
            <td>${saloon.name}</td>
            <td>${saloon.capacity}</td>
			<td>${saloon.name}</td>
            <td>${saloon.capacity}</td>
            <td>
                <a href="editSaloons?id=${saloon.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="deleteSaloons?id=${saloon.id}">Delete</a>
                 
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>