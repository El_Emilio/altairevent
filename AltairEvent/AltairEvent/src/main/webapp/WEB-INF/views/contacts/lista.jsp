<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALtair Event</title>
</head>
<body>
<div align="center">
    <h2>ALtair Event. Contacts</h2>
    <form method="get" action="searchContacts">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="newContacts">New Contacts</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Number</th>
            <th>Location</th>
            <th>Customer</th>

        </tr>
        <c:forEach items="${listContacts}" var="contacts">
        <tr>
            <td>${contacts.id}</td>
            <td>${contacts.name}</td>
            <td>${contacts.surname}</td>
            <td>${contacts.number}</td>
            <td>${contacts.location}</td>
            <td>${contacts.customer}</td>
            <td>
                <a href="editContacts?id=${contacts.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="deleteContacts?id=${contacts.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>