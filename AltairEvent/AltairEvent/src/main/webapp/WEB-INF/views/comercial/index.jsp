<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALtair Event</title>
</head>
<body>
<div align="center">
    <h2>ALtair Event. Commercial</h2>
    <form method="get" action="searchCommercial">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="newCommercial">New Commercial</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>E-mail</th>
            <th>phone</th>
            <th>age</th>
			<th>WorkStation</th>
        </tr>
        <c:forEach items="${listCommercial}" var="commercial">
        <tr>
            <td>${commercial.id}</td>
            <td>${commercial.name}</td>
            <td>${commercial.surname}</td>
            <td>${commercial.email}</td>
            <td>${commercial.phone}</td>
            <td>${commercial.age}</td>
            <td>${commercial.workStation}</td>
            <td>
                <a href="editCommercial?id=${commercial.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="deleteCommercial?id=${commercial.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>