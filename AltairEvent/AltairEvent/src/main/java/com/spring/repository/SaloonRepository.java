package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spring.models.Saloons;

public interface SaloonRepository {
	@Query(value = "SELECT r FROM Saloons s "
			+ "WHERE s.name LIKE '%' ||:keyword || '%'"
			+ " OR s.capacity LIKE '%' || :keyword || '%'")
	public List<Saloons> search(@Param("keyword") String keyword);
	
}
