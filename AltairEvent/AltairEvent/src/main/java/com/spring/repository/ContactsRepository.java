package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spring.models.Contacts;


public interface ContactsRepository {


	

	@Query(value = "SELECT c FROM Contacts c "
			+ "WHERE c.name LIKE '%' ||:keyword || '%'"
			+ " OR c.email LIKE '%' || :keyword || '%'"
			+ " OR c.nick LIKE '%' || :keyword || '%'"
			+ " OR c.surname LIKE '%' || :keyword || '%'"
			+ " OR c.phone LIKE '%' || :keyword || '%'")
	public List<Contacts> search(@Param("keyword") String keyword);
}
