package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.spring.models.Commercial;

public class CommercialRepository {
	
	@Query(value = "SELECT m FROM Commercial c "
			+ "WHERE c.name LIKE '%' ||:keyword || '%'"
			+ " OR c.email LIKE '%' || :keyword || '%'"
			+ " OR c.nick LIKE '%' || :keyword || '%'"
			+ " OR c.surname LIKE '%' || :keyword || '%'"
			+ " OR c.phone LIKE '%' || :keyword || '%'")
	public List<Commercial> search(@Param("keyword") String keyword);
	
	
}
