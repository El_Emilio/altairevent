package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.spring.models.Services;

public interface ServiceRepository {

	@Query (value ="Select s from Services s where s.active = true")
	public List<Services> ServicesActive();
}
