package com.spring.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.models.Events;

public interface EventRepository {
	@Autowired
	EventRepository eventRepository;
	
	public void save(Events event) {
		eventRepository.save(event);
	}
	
	
	public List<Events> listAll(){
		return (List<Events>) eventRepository.findAll();
	}
	
	public Events get(Long id) {
		return eventRepository.findById(id).get();
	}
	
	public void delete(Long id) {
		eventRepository.deleteById(id);
	}
	
	public List<Events> search(String keyword){
		return eventRepository.search(keyword);
	}
	
	public List<Events> consultarDisponible(){
		return eventRepository.consultarDisponible();
	}
	
	public List<Event> consultarDisponiblesConFechas(String fecha1, String fecha2){
		return eventRepository.consultarDisponiblesConFechas(fecha1, fecha2);
	}
	
	public Long eventosFuturos(Long id){
        return eventRepository.eventosFuturos(id);
    }
	
	public Long eventHave(Long id) {
		return eventRepository.eventsHave(id);
	}
	
	public List<Events> EventoFiltrado(Long id){
		return eventRepository.listaEventosFiltrado(id);
	}
}
