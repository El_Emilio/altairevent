package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Customer;

public interface CustomerRepository {

	@Query("select COUNT(1) from Customer c where c.nick=?1 and c.password=?2")
	public long pregunta(String nick,String password);
	
	//@Query(nativeQuery = true,value = "SELECT count(1) from CUSTOMER WHERE nick LIKE '"+nick+"' AND password LIKE '"+password+"'  ")
	//long getTotalCount();
	
	@Query(value = "SELECT c FROM Customer c WHERE c.nick=:nick and c.password=:password")
	public Customer  dameCustomerLogado(@Param("nick") String nick, @Param("password") String password);

	@Query(value = "SELECT c FROM Customer c "
			+ "WHERE c.name LIKE '%' ||:keyword || '%'"
			+ " OR c.email LIKE '%' || :keyword || '%'"
			+ " OR c.nick LIKE '%' || :keyword || '%'"
			+ " OR c.surname LIKE '%' || :keyword || '%'"
			+ " OR c.phone LIKE '%' || :keyword || '%'")
	public List<Customer> search(@Param("keyword") String keyword);
}
