package com.spring.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "SALOONS")
public class Saloons extends DomainEntity {

	public String name;
	public int capacity;
	public boolean available;
	public int price;
	
	public List<Events> eventss;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	@OneToMany(mappedBy = "saloons", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@Valid
	@NotNull
	public List<Events> getEventss() {
		return eventss;
	}

	public void setEventss(List<Events> eventss) {
		this.eventss = eventss;
	}
}
