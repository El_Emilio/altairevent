package com.spring.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "CUSTOMER")
public class Customer extends Person {

	public String document;
	public String adress;
	public String sector;
	
	private List<Contacts> contactss;
	public List<Events> eventss;
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@Valid
	@NotNull
	public List<Contacts> getContactss() {
		return contactss;
	}
	public void setContactss(List<Contacts> contactss) {
		this.contactss = contactss;
	}
	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@Valid
	@NotNull
	public List<Events> getEventss() {
		return eventss;
	}
	public void setEventss(List<Events> eventss) {
		this.eventss = eventss;
	}
	public Customer(String document, String adress, String sector, List<Contacts> contactss, List<Events> eventss) {
		super();
		this.document = document;
		this.adress = adress;
		this.sector = sector;
		this.contactss = contactss;
		this.eventss = eventss;
	}
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Customer(String name, String surname, String email, String phone, int age) {
		super(name, surname, email, phone, age);

	}
}
