package com.spring.models;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "EVENTS")
public class Events extends DomainEntity{

	public Date startDate;
	public Date endDate;
	public int assistance;
	public String name;
	public String organizationCompany;
	public Long totalPrice;
	
	public Services services;
	
	public Commercial commercial;
	
	public Customer customer;

	private Saloons saloon;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getAssistance() {
		return assistance;
	}

	public void setAssistance(int assistance) {
		this.assistance = assistance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganizationCompany() {
		return organizationCompany;
	}

	public void setOrganizationCompany(String organizationCompany) {
		this.organizationCompany = organizationCompany;
	}

	public Long getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}
@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Services getServices() {
		return services;
	}

	public void setServices(Services services) {
		this.services = services;
	}
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Commercial getCommercial() {
		return commercial;
	}

	public void setCommercial(Commercial commercial) {
		this.commercial = commercial;
	}
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Saloons getSaloon() {
		return saloon;
	}

	public void setSaloon(Saloons saloon) {
		this.saloon = saloon;
	}
}
