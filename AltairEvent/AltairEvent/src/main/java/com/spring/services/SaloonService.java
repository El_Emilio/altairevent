package com.spring.services;

import java.util.List;

import com.spring.models.Saloons;
import com.spring.repository.SaloonRepository;

public class SaloonService {
	private SaloonRepository repository;
	public List<Saloons> listAll(){
		return (List<Saloons>) repository.findAll();
	}
	
	public Saloons get(Long id) {
		return repository.findById(id).get();
	}
	
	public Saloons create() {
		Saloons result = new Saloons();
		return result;
		
	}
	public Saloons save(Saloons Saloons) {
		Saloons result = this.repository.save(Saloons);
		return result;
		
	}
	public void delete(Saloons Saloons) {
		this.repository.delete(Saloons);
	}
	public void delete(long id) {
		this.repository.deleteById(id);
	}
	public List<Saloons> search(String keyword) {
		return repository.search(keyword);
	}
}
