package com.spring.services;

import java.util.List;

import com.spring.models.Events;
import com.spring.repository.CommercialRepository;
import com.spring.repository.EventRepository;


public class EventService {
	 private EventRepository repository;
	
	public List<Events> listAll(){
		return (List<Events>) repository.findAll();
	}
	
	public Events get(Long id) {
		return repository.findById(id).get();
	}
	
	public Events create() {
		Events result = new Events();
		return result;
		
	}
	public Events save(Events Events) {
		Events result = this.repository.save(Events);
		return result;
		
	}
	public void delete(Events Events) {
		this.repository.delete(Events);
	}
	public void delete(long id) {
		this.repository.deleteById(id);
	}
	public List<Events> search(String keyword) {
		return repository.search(keyword);
	}
	
}
