package com.spring.services;

import java.util.List;

import com.spring.models.Services;
import com.spring.repository.ServiceRepository;

public class ServiceService {

	ServiceRepository serviceRepository;
	
	public void save(Services Services) {
		serviceRepository.save(Services);
	}
	
	
	public List<Services> listAll(){
		return (List<Services>) serviceRepository.findAll();
	}
	
	public Services get(Long id) {
		return serviceRepository.findById(id).get();
	}
	
	public void delete(Long id) {
		serviceRepository.deleteById(id);
	}
	
	public List<Services> search(String keyword){
		return serviceRepository.search(keyword);
	}
	public List<Services> ServicesActive(){
		return serviceRepository.ServicesActive();
	}
}
