package com.spring.services;

import java.util.List;

import com.spring.models.Commercial;
import com.spring.repository.CommercialRepository;

public class CommercialService {

	 private CommercialRepository repository;
		
		public List<Commercial> listAll(){
			return (List<Commercial>) repository.findAll();
		}
		
		public Commercial get(Long id) {
			return repository.findById(id).get();
		}
		
		public Commercial create() {
			Commercial result = new Commercial();
			return result;
			
		}
		public Commercial save(Commercial Commercial) {
			Commercial result = this.repository.save(Commercial);
			return result;
			
		}
		public void delete(Commercial Commercial) {
			this.repository.delete(Commercial);
		}
		public void delete(long id) {
			this.repository.deleteById(id);
		}
		public List<Commercial> search(String keyword) {
			return repository.search(keyword);
		}
		public long search(String usu, String clave) {
			return repository.pregunta(usu, clave);
			
		}
		
}
