package com.spring.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.models.Customer;
import com.spring.repository.CustomerRepository;

@Service
public class CustomerService {

	private CustomerRepository repo; 

	public void save(Customer customer) {
		repo.save(customer);
	}
	
	public List<Customer> listAll(){
		return (List<Customer>) repo.findAll();
	}
	
	public Customer get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
	
	public List<Customer> search(String keyword) {
		return repo.search(keyword);
	}
	public long search(String usu, String clave) {
		return repo.pregunta(usu, clave);
	}

	public Customer dameCustomer(String nick, String password) {
		return repo.dameCustomerLogado(nick, password);
		
	}
}
