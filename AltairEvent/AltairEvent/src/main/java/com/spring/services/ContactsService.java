package com.spring.services;

import java.util.List;

import com.spring.models.Contacts;
import com.spring.models.Customer;
import com.spring.repository.ContactsRepository;
import com.spring.repository.CustomerRepository;

public class ContactsService {

	private ContactsRepository repo; 

	public void save(Contacts Contacts) {
		repo.save(Contacts);
	}
	
	public List<Contacts> listAll(){
		return (List<Contacts>) repo.findAll();
	}
	
	public Contacts get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
	
	public List<Contacts> search(String keyword) {
		return repo.search(keyword);
	}
	public long search(String usu, String clave) {
		return repo.pregunta(usu, clave);
	}

	
	
}
