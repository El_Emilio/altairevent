package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Services;
import com.spring.services.EventService;
import com.spring.services.ServiceService;

public class ServicesControllers {
	ServiceService serviceServices;
	EventService eventServices;
	@RequestMapping("serviceHome")
	public ModelAndView serviceHome() {
		List<Services> listServices = serviceServices.listAll();
		ModelAndView modelAndView = new ModelAndView("service/index");
		modelAndView.addObject("listServices", listServices);
		return modelAndView;
	}
	
	@RequestMapping("serviceNew")
	public String newServiceForm(Map<String,Object> model) {
		Services service = new Services();
		model.put("service", service);
		return "service/new-service";
		
	}
	
	@RequestMapping(value = "serviceSave", method = RequestMethod.POST)
	public ModelAndView serviceSave(@ModelAttribute("service") Services Services) {
		ModelAndView modelAndView = new ModelAndView("service/index");
	     if(eventServices.eventosFuturos(Services.getId())>0) {
	    	 Services.setActive(true);
	            modelAndView = new ModelAndView("service/edit-service");
	            modelAndView.addObject("errorMessage", "No puedes cambiar el activo del servicio porque tiene un evento futuro");
	            return modelAndView;
	        }
	      
		serviceServices.save(Services);
		return modelAndView;
	}
	
	@RequestMapping("serviceEdit")
	public ModelAndView serviceEdit(@RequestParam Long id) {
		ModelAndView modelAndView = new ModelAndView("service/edit-service");
		Services service = serviceServices.get(id);
		
		
		modelAndView.addObject("service",service);
		return modelAndView;
	}
	
	@RequestMapping("serviceDelete")
	public ModelAndView serviceDelete(@RequestParam Long id) {
		
		List<Services> listService = serviceServices.listAll();
		ModelAndView modelAndView = new ModelAndView("service/index");
		 
		
		serviceServices.delete(id);
		listService = serviceServices.listAll();
		 modelAndView.addObject("listService",listService);
		 return modelAndView;
	}
	
	 @RequestMapping("serviceSearch")
	 public ModelAndView serviceSearch(String keyword){
		 List<Services> listService = serviceServices.search(keyword);
		 ModelAndView modelAndView =  new ModelAndView("service/search");
		 modelAndView.addObject("listService",listService);
		 return modelAndView;
	 }
}
