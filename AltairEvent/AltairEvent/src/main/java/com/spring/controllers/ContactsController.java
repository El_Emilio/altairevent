package com.spring.controllers;

import java.util.List;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Contacts;
import com.spring.services.ContactsService;
public class ContactsController {
	
	private ContactsService ContactsService;
	public ModelAndView home() {
		List<Contacts> listContacts = ContactsService.listAll();
		ModelAndView modelAndView = new ModelAndView("contacts/index");
		modelAndView.addObject("listContacts",listContacts);
		return modelAndView;
		
	}
	@RequestMapping("newRegister")
	public String newRegister( Model modelo) {
		Contacts Contacts = new Contacts();
		modelo.addAttribute("Contacts", Contacts);
	return "contacts/new_contacts";
	}
		

	@RequestMapping("newContacts")
	public String newContactsForm(Model modelo) {
		Contacts Contacts = new Contacts();
		modelo.addAttribute("Contacts", Contacts);
	return "contacts/new_contacts";
	}

	@RequestMapping(value = "saveContacts", method = RequestMethod.POST)
	public String save(@ModelAttribute("customer") Contacts Contacts) {
		ContactsService.save(Contacts);
	return("redirect:/listContacts"); 
	}
	
	@RequestMapping("editContacts")
	public ModelAndView editContactsForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("contacts/edit_contacts");
		Contacts Contacts = ContactsService.get(id);
		mav.addObject("Contacts", Contacts);
		return mav;
	}
	
	@RequestMapping("deleteContacts")
	public String deleteContactsForm(@RequestParam long id) {
		ContactsService.delete(id);
	return("redirect:/listContacts");
	}
	
	@RequestMapping("searchContacts")
	public ModelAndView findById(@RequestParam String keyword) {
	List<Contacts> result = ContactsService.search(keyword);
	ModelAndView mav = new ModelAndView("search");
	mav.addObject("result", result);
	return mav;
	}
	
}
