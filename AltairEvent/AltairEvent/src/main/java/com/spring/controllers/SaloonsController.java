package com.spring.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Saloons;
import com.spring.services.SaloonService;

public class SaloonsController {
	private SaloonService SaloonService;
	
	@RequestMapping("listSaloons")
	public ModelAndView listRoom() {
		List<Saloons> listSaloons = SaloonService.listAll();
		ModelAndView mav = new ModelAndView("saloon/lista");
		mav.addObject("listSaloon", listSaloons);
		return mav;
	}
	
	@RequestMapping("newSaloons")
	public ModelAndView newRoomForm() {
		ModelAndView mav = new ModelAndView("saloon/new_saloon");
		Saloons Saloons = new Saloons();
		mav.addObject(Saloons);
	return mav;
	}
	
	@RequestMapping(value = "saveSaloons", method = RequestMethod.POST)
	public String saveRoom(@ModelAttribute("Saloons") Saloons Saloons) {
	SaloonService.save(Saloons);

	return("redirect:/listSaloons");
	
	}
	
	
	@RequestMapping("editSaloons")
	public ModelAndView editRoomForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("saloon/edit_saloon");
		Saloons Saloons = SaloonService.get(id);
		mav.addObject("Saloons", Saloons);
		return mav;
	}
	
	@RequestMapping("deleteSaloons")
	public String deleteRoomForm(@RequestParam long id) {
	SaloonService.delete(id);
	return("redirect:/listSaloon");
	}
	
	@RequestMapping("searchSaloons")
	public ModelAndView search(@RequestParam String keyword) {
	List<Saloons> result = SaloonService.search(keyword);
	ModelAndView mav = new ModelAndView("saloon/lista");
	mav.addObject("listsaloon", result);
	return mav;
	}
	
}
