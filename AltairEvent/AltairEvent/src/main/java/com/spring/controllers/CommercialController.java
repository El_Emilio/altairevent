package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Commercial;
import com.spring.services.CommercialService;

public class CommercialController {
	private CommercialService commercialService;
	
	@RequestMapping("listMonitor")
	public ModelAndView listMonitor() {
	
		List<Commercial> listCommercial = commercialService.listAll();
		ModelAndView mav = new ModelAndView("commercial/lista");
		mav.addObject("listCommercial", listCommercial);
		return mav;
	}
	
	@RequestMapping("loginCommercial")
	public ModelAndView loginCommercial() {
		List<Commercial> listCommercial = commercialService.listAll();
		ModelAndView mav = new ModelAndView("commercial/lista");
		mav.addObject("listCommercial", listCommercial);
		return mav;
	}
	
	@RequestMapping("newCommercial")
	public String newCommercialForm(Map<String, Object> model) {
		Commercial Commercial = new Commercial();
	model.put("Commercial", Commercial);
	return "commercial/new_commercial";
	}
	
	@RequestMapping(value = "saveCommercial", method = RequestMethod.POST)
	public String saveCommercial(@ModelAttribute("Commercial") Commercial Commercial) {
		commercialService.save(Commercial);
	return("redirect:/listCommercial");
	}
	
	
	@RequestMapping("editCommercial")
	public ModelAndView editCommercialForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("commercial/edit_commercial");
		Commercial Commercial = commercialService.get(id);
		mav.addObject("Commercial", Commercial);
		return mav;
	}
	
	@RequestMapping("deleteCommercial")
	public String deleteCommercialForm(@RequestParam long id) {
		commercialService.delete(id);
	return("redirect:/listCommercial");
	}
	
	@RequestMapping("searchCommercial")
	public ModelAndView search(@RequestParam String keyword) {
	List<Commercial> result = commercialService.search(keyword);
	ModelAndView mav = new ModelAndView("commercial/lista");
	mav.addObject("listCommercial", result);
	return mav;
	}
}
