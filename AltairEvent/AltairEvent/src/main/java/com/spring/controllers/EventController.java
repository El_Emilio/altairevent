package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.spring.models.Events;

import com.spring.services.EventService;

public class EventController {
	private EventService eventService;
	
	
	@RequestMapping("listMonitor")
	public ModelAndView listEvents() {
	
		List<Events> listEvents = eventService.listAll();
		ModelAndView mav = new ModelAndView("events/lista");
		mav.addObject("listEvents", listEvents);
		return mav;
	}
	@RequestMapping("newEvents")
	public String newEventsForm(Map<String, Object> model) {
		Events Events = new Events();
	model.put("Events", Events);
	return "events/new_events";
	}
	
	@RequestMapping(value = "saveEvents", method = RequestMethod.POST)
	public String saveEvents(@ModelAttribute("Events") Events Events) {
		eventService.save(Events);
	return("redirect:/listEvents");
	}
	
	
	@RequestMapping("editEvents")
	public ModelAndView editEventsForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("events/edit_events");
		Events Events = eventService.get(id);
		mav.addObject("Events", Events);
		return mav;
	}
	
	@RequestMapping("deleteEvents")
	public String deleteEventsForm(@RequestParam long id) {
		eventService.delete(id);
	return("redirect:/listEvents");
	}
	
}
