package com.spring.controllers;

import java.util.List;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Customer;
import com.spring.services.CustomerService;
import com.spring.services.SaloonService;

public class CustomerController {
	private CustomerService CustomerService;
	public ModelAndView home() {
		List<Customer> listCustomer = CustomerService.listAll();
		ModelAndView modelAndView = new ModelAndView("customer/index");
		modelAndView.addObject("listCustomer",listCustomer);
		return modelAndView;
		
	}
	@RequestMapping("newRegister")
	public String newRegister( Model modelo) {
		Customer customer = new Customer();
		modelo.addAttribute("customer", customer);
	return "customer/new_register";
	}
		

	@RequestMapping("newCustomer")
	public String newCustomerForm(Model modelo) {
		Customer customer = new Customer();
		modelo.addAttribute("customer", customer);
	return "customer/new_customer";
	}

	@RequestMapping(value = "saveCustomer", method = RequestMethod.POST)
	public String save(@ModelAttribute("customer") Customer customer) {
		CustomerService.save(customer);
	return("redirect:/listCustomer"); 
	}
	
	@RequestMapping("editCustomer")
	public ModelAndView editCustomerForm(@RequestParam long id) {
		ModelAndView mav = new ModelAndView("customer/edit_customer");
		Customer customer = CustomerService.get(id);
		mav.addObject("customer", customer);
		return mav;
	}
	
	@RequestMapping("deleteCustomer")
	public String deleteCustomerForm(@RequestParam long id) {
		CustomerService.delete(id);
	return("redirect:/listCustomer");
	}
	
	@RequestMapping("searchCustomer")
	public ModelAndView findById(@RequestParam String keyword) {
	List<Customer> result = CustomerService.search(keyword);
	ModelAndView mav = new ModelAndView("search");
	mav.addObject("result", result);
	return mav;
	}
	
}
